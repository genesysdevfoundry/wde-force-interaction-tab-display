# README #

Workspace Desktop Edition Plugin sample, describing how to force the opening and display of a selected custom tab, on new interactions.

This sample is a variation of the InteractionExtensionSample (Genesyslab.Desktop.Modules.InteractionExtensionSample) that is shipped with Workspace Desktop Edition Developer's installation.
It introduces a "SideButtonManager" that plays with some agent profile data to force selection and opening of custom vertical tab (on interaction created).
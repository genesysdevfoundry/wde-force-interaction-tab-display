﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genesyslab.Desktop.Modules.InteractionExtensionSample
{
    public interface ISideButtonManager
    {
        void ForceDefaultSideViewSelectionAtNextInteraction(string interactionType);
    }
}

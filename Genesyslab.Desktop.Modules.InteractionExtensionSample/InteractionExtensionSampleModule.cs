using Genesyslab.Desktop.Infrastructure.Commands;
using Genesyslab.Desktop.Infrastructure.DependencyInjection;
using Genesyslab.Desktop.Infrastructure.ViewManager;
using Genesyslab.Desktop.Modules.InteractionExtensionSample.MySample;
using Genesyslab.Desktop.Infrastructure;
using Genesyslab.Desktop.Modules.Core.Model.Interactions;
using System;
using System.Text;
using Genesyslab.Platform.Commons.Logging;
//using Microsoft.Practices.Composite.Modularity;

namespace Genesyslab.Desktop.Modules.InteractionExtensionSample
{
	/// <summary>
	/// This class is a sample module which shows several ways of customization
	/// </summary>
	public class InteractionExtensionSampleModule : IModule
	{
		readonly IObjectContainer container;
		readonly IViewManager viewManager;
		readonly ICommandManager commandManager;
        ILogger log;

        ISideButtonManager sideButtonManager;

		/// <summary>
		/// Initializes a new instance of the <see cref="InteractionExtensionSampleModule"/> class.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="viewManager">The view manager.</param>
		/// <param name="commandManager">The command manager.</param>
		public InteractionExtensionSampleModule(IObjectContainer container, IViewManager viewManager, ICommandManager commandManager, ILogger log)
		{
			this.container = container;
			this.viewManager = viewManager;
			this.commandManager = commandManager;
            this.log = log.CreateChildLogger("InteractionExtensionSampleModule");
        }

		/// <summary>
		/// Initializes the module.
		/// </summary>
		public void Initialize()
		{
			// Add a view in the right panel in the interaction window

			// Here we register the view (GUI) "IMySampleView" and its behavior counterpart "IMySampleViewModel"
			container.RegisterType<IMySampleView, MySampleView>();
			container.RegisterType<IMySampleViewModel, MySampleViewModel>();
            container.RegisterType<ISideButtonManager, SideButtonManager>(true);

			// Put the MySample view in the region "InteractionWorksheetRegion"
			viewManager.ViewsByRegionName["InteractionWorksheetRegion"].Add(
				new ViewActivator() { ViewType = typeof(IMySampleView), ViewName = "MyInteractionSample", ActivateView = true }
			);

			// Here we register the view (GUI) "IMySampleButtonView"
			container.RegisterType<IMySampleButtonView, MySampleButtonView>();

			// Put the MySampleMenuView view in the region "CaseViewSideButtonRegion" (The case toggle button in the interaction windows)
			viewManager.ViewsByRegionName["CaseViewSideButtonRegion"].Add(
				new ViewActivator() { ViewType = typeof(IMySampleButtonView), ViewName = "MySampleButtonView", ActivateView = true }
			);

            sideButtonManager = container.Resolve<ISideButtonManager>();
		}
    }
}

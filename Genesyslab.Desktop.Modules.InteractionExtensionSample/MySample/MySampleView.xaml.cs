﻿using System;
using System.Windows.Controls;
using Genesyslab.Desktop.Modules.Windows.Common.DimSize;
using System.ComponentModel;
using System.Windows;
using Genesyslab.Desktop.Infrastructure.DependencyInjection;

namespace Genesyslab.Desktop.Modules.InteractionExtensionSample.MySample
{
	/// <summary>
	/// Interaction logic for MySampleView.xaml
	/// </summary>
	public partial class MySampleView : UserControl, IMySampleView
	{
		readonly IObjectContainer container;

		public MySampleView(IMySampleViewModel mySampleViewModel, IObjectContainer container )
		{
			this.Model = mySampleViewModel;
			this.container = container;

			InitializeComponent();

			Width = Double.NaN;
			Height = Double.NaN;
			MinSize = new MSize() { Width = 1000.0, Height = 1000.0 };
		}

		MSize _MinSize;
		public MSize MinSize
		{
			get { return _MinSize; }  // (MSize)base.GetValue(MinSizeProperty); }
			set
			{
				_MinSize = value; // base.SetValue(MinSizeProperty, value);
				OnPropertyChanged("MinSize");
			}
		}


		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string name)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}

		#endregion
		#region IMySampleView Members

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		/// <value>The model.</value>
		public IMySampleViewModel Model
		{
			get { return this.DataContext as IMySampleViewModel; }
			set { this.DataContext = value; }
		}

		#endregion

		#region IView Members

		/// <summary>
		/// Gets or sets the context.
		/// </summary>
		/// <value>The context.</value>
		public object Context { get; set; }

		/// <summary>
		/// Creates this instance.
		/// </summary>
		public void Create()
		{
		}

		/// <summary>
		/// Destroys this instance.
		/// </summary>
		public void Destroy()
		{
		}

		#endregion

		private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Model.ResetCounter();
		}

		private void MySampleView_Loaded(object sender, RoutedEventArgs e)
		{
		}

	}
}

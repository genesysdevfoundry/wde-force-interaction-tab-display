﻿using Genesyslab.Desktop.Infrastructure;
using Genesyslab.Desktop.Modules.Core.Model.Interactions;
using Genesyslab.Desktop.Modules.Windows;
using Genesyslab.Platform.Commons.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genesyslab.Desktop.Modules.InteractionExtensionSample
{
    public class SideButtonManager : ISideButtonManager
    {
        readonly IInteractionManager interactionManager;
        ILogger log;

        private readonly string defaultSideViewName = "MyInteractionSample";

        public SideButtonManager(IInteractionManager interactionManager, ILogger log)
        {
            this.log = log.CreateChildLogger("SideButtonManager");
            this.interactionManager = interactionManager;
            this.interactionManager.InteractionCreated += new EventHandler<EventArgs<IInteraction>>(eventHandler_InteractionCreated);
        }

        public void ForceDefaultSideViewSelectionAtNextInteraction(string interactionType)
        {
            WindowsOptions.Default.SetInteractionWindowLastInformationPanel(interactionType, defaultSideViewName);
            WindowsOptions.Default.SetInteractionWindowShowInformationPanel(interactionType, true);
        }

        public void eventHandler_InteractionCreated(object sender, EventArgs<IInteraction> e)
        {
            try
            {
                IInteraction interaction = e.Value;

                if (interaction != null)
                {
                    StringBuilder sb = new StringBuilder("InteractionCreated (Workspace interface) InteractionId=" + interaction.InteractionId);
                    sb.Append("\n\ttype=" + interaction.Type);
                    sb.Append("\n\tmedia=" + interaction.Media.Name);
                    log.Debug(sb.ToString());

                    ForceDefaultSideViewSelectionAtNextInteraction(interaction.Type.ToString());
                }
            }
            catch (Exception exception)
            {
                log.Error("InteractionCreated Exception=" + exception.Message);
            }
        }
    }
}
